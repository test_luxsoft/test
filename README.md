**The task was set to:**
1. Create an account in AWS;
2. Create a Terraform recipe that deploys 2 ec2 instances (t2.micro) instances in AWS (Zone of your choice);
3. Create ansible recipe that automatically deploys 1 instance of docker container with nginx on both instances, and configures SWAP on them.


**setup an EC2 machine with SSH access**
- install Docker in this VM
- create a new user with sudo access
- Create a Docker group
- Add the new user in the Docker group
- Pull docker images nginx
- Run nginx container

**Pre-requisites:**

1. Awscli, Terraform and Ansible are installed in your device (please see specific documentations for that)
2. You logged in with awscli on your device
The steps are below:

**Generate ssh public and private keys in the tf folder**

**We will need these keys to login to the EC2 machine.**
```
$ cd tf
$ mkdir ssh-keys
$ cd ssh-keys
$ ssh-keygen -t rsa -f aws_ssh_key
```
Prepare the infrastructure with terraform
```
$ cd tf
$ terraform init
$ terraform apply
```

**You need to add ip-address to the file /andible/inventory/host**

Provision your new EC2 instance with Docker
```
$ cd ansible
$ ansible-playbook simpleplaybook.yml
```
Check if it worked
By default, your_user is ubuntu.
```
$ ssh -i "your_ssh_key" your_user@your_elastic_ip_address
$ docker
$ docker ps
```
If no errors occured, it meaned everything worked.

Stop everything
```
$ cd tf
$ terraform destroy
```