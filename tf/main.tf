terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 2.7"
    }
  }
}

provider "aws" {
  profile = "default"
  region  = "eu-central-1"
}


resource "aws_key_pair" "setup_key" {
  key_name   = "aws-key"
  public_key = file(var.key_pair_path["public_key_path"])
}

resource "aws_instance" "ubuntu_vm1" {
  ami           = "ami-0932440befd74cdba"
  instance_type = "t2.micro"
  key_name      = "aws-key"
  associate_public_ip_address = false

  root_block_device {
    delete_on_termination = true
    encrypted             = false
    volume_size           = 8
    volume_type           = "gp2"
  }
}

resource "aws_eip" "ip" {
  vpc      = true
  instance = aws_instance.ubuntu_vm1.id
}

resource "aws_instance" "ubuntu_vm2" {
  ami           = "ami-0932440befd74cdba"
  instance_type = "t2.micro"
  key_name      = "aws-key"
  associate_public_ip_address = false

  root_block_device {
    delete_on_termination = true
    encrypted             = false
    volume_size           = 8
    volume_type           = "gp2"
  }
}

resource "aws_eip" "ip2" {
  vpc      = true
  instance = aws_instance.ubuntu_vm2.id
}


resource "local_file" "ip" {
  content  = aws_eip.ip.public_ip
  filename = "./output/eip.txt"
}
